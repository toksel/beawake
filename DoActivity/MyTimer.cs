﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DoActivity
{
    class MyTimer : System.Timers.Timer
    {
        public MyTimer(double interval) : base(interval) { }
        public MyTimer() : base() { }

        public object Tag { get; set; }
    }
}
