﻿namespace DoActivity
{
    class ExtensionMethods
    {
        public static bool ConvertToBool(bool? b)
        {
            if (b != null && b != false)
            {
                return true;
            }
            return false;
        }
    }
}
