﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Winforms = System.Windows.Forms;

namespace DoActivity
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("User32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private static MyTimer aTimer;

        public MainWindow()
        {
            InitializeComponent();
            txt_box_period.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, PasteCommand));
            txt_box_delay.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, PasteCommand));
        }

        private void btn_move_mouse_rdp_Click(object sender, RoutedEventArgs e)
        {        
            int period = 10000;
            if (int.TryParse(txt_box_period.Text, out period))
            {
                period = (period < 10) ? 10000 : period * 1000;
            }

            int delay = 500;
            int.TryParse(txt_box_delay.Text, out delay);



            TimeDelays td = new TimeDelays(period, delay, Tasks.MoveOnRdp);

            SetTimer(td);
        }

        private void btn_move_mouse_screen_Click(object sender, RoutedEventArgs e)
        {
            int period = 10000;
            if (int.TryParse(txt_box_period.Text, out period))
            {
                period = (period < 10) ? 10000 : period * 1000;
            }

            int delay = 500;
            int.TryParse(txt_box_delay.Text, out delay);



            TimeDelays td = new TimeDelays(period, delay, Tasks.MoveOnScreen);

            SetTimer(td);
        }

        private void btn_stop_Click(object sender, RoutedEventArgs e)
        {
            aTimer?.Stop();
            aTimer?.Dispose();
        }

        private void btn_exit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private static void SetTimer(TimeDelays td)
        {
            aTimer = new MyTimer(td.Period)
            {
                Tag = td.Delay as object
            };
            // Hook up the Elapsed event for the timer. 
            switch (td.TaskType)
            {
                case Tasks.MoveOnScreen:
                    aTimer.Elapsed += OnTimedEventScreen;
                    break;
                case Tasks.MoveOnRdp:
                    aTimer.Elapsed += OnTimedEventRDP;
                    break;
                default:
                    break;
            }          
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void OnTimedEventRDP(Object sender, ElapsedEventArgs e)
        {
            System.Drawing.Size resolution = Winforms.Screen.PrimaryScreen.Bounds.Size;
            MyTimer timer = sender as MyTimer;
            int delay = (int)timer.Tag;

            string rdpProcessName = "";
            var reg = new Regex("- Remote Desktop Connection", RegexOptions.IgnoreCase);

            foreach (var process in Process.GetProcesses())
            {
                if (!string.IsNullOrEmpty(process.MainWindowTitle.ToString()))
                {
                    if (reg.IsMatch(process.MainWindowTitle.ToString()))
                    {
                        rdpProcessName = process.MainWindowTitle.ToString();
                        break;
                    }
                }
            }

            // stop if rdp is out                 
            if (string.IsNullOrEmpty(rdpProcessName))
            {
                aTimer.Stop();
                aTimer.Dispose();
                return;
            }

            int mousepositionX = Winforms.Cursor.Position.X;
            int mousepositionY = Winforms.Cursor.Position.Y;

            Winforms.Cursor.Position = new System.Drawing.Point(20, resolution.Height - 20);

            // expand rdp 
            IntPtr rdp = FindWindow(null, rdpProcessName);
            ShowWindow(rdp, 10);

            Winforms.Cursor.Position = new System.Drawing.Point(resolution.Width / 2, resolution.Height / 2);
            Thread.Sleep(delay / 2);
            Winforms.Cursor.Position = new System.Drawing.Point(resolution.Width / 2 + 150, resolution.Height / 2 + 150);
            Thread.Sleep(delay / 2);
            Winforms.Cursor.Position = new System.Drawing.Point(resolution.Width / 2 - 150, resolution.Height / 2 - 150);
            Thread.Sleep(delay);
            ShowWindow(rdp, 6);
            Winforms.Cursor.Position = new System.Drawing.Point(mousepositionX, mousepositionY);
        }

        private static void OnTimedEventScreen(Object sender, ElapsedEventArgs e)
        {
            System.Drawing.Size resolution = Winforms.Screen.PrimaryScreen.Bounds.Size;
            MyTimer timer = sender as MyTimer;
            int delay = (int)timer.Tag;

            int mousepositionX = Winforms.Cursor.Position.X;
            int mousepositionY = Winforms.Cursor.Position.Y;

            Winforms.Cursor.Position = new System.Drawing.Point(20, resolution.Height - 20);

            Winforms.Cursor.Position = new System.Drawing.Point(resolution.Width / 2, resolution.Height / 2);
            Thread.Sleep(delay / 2);
            Winforms.Cursor.Position = new System.Drawing.Point(resolution.Width / 2 + 150, resolution.Height / 2 + 150);
            Thread.Sleep(delay / 2);
            Winforms.Cursor.Position = new System.Drawing.Point(resolution.Width / 2 - 150, resolution.Height / 2 - 150);
            Thread.Sleep(delay);
            Winforms.Cursor.Position = new System.Drawing.Point(mousepositionX, mousepositionY);
        }

        private void txt_box_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            string pattern = "[^0-9]";
            if (Regex.IsMatch(e.Text, pattern))
            {
                e.Handled = true;
            }
        }
        private void txt_box_PreviewSpacePressed(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        public static void PasteCommand(object sender, ExecutedRoutedEventArgs e)
        {
            string paste = Clipboard.GetText();
            paste = paste.Trim();
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ",";
            int num;
            if (int.TryParse(paste, out num))
            {
                Convert.ToInt32(paste, provider);
                TextBox thisTextBox = sender as TextBox;
                thisTextBox.Text = paste;
            }
            else
            {
                TextBox thisTextBox = sender as TextBox;
                thisTextBox.Text = string.Empty;
            }
        }
    }
}
