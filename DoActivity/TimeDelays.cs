﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DoActivity
{
    public enum Tasks
    {
        MoveOnScreen,
        MoveOnRdp
    }

    class TimeDelays
    {
        public int Period { get; set; }
        public int Delay { get; set; }
        public Tasks TaskType { get; set; }       
        public TimeDelays(int period, int delay, Tasks taskType)
        {
            Period = period;
            Delay = delay;
            TaskType = taskType;
        }

    }
}
